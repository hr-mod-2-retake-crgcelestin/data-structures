# Write your code here to make the tests pass.
#
# Change your working directory to this directory,
# circular_queue.
#
# # Start by running python -m pytest tests/test_01.py and
# making the test pass.
#
# Then, run python -m pytest tests/test_02.py to make the
# next test pass. Keep going to tests/test_XX.py.

class CircularQueue():
    def __init__(self, size, head=0, tail=0, length=0):
        self.length=length
        self.size=size
        self.head=head
        self.tail=tail
        self.buffer=[None]*size
    def enqueue(self, val):
        self.length+=1
        self.tail=self.length
        self.buffer[self.tail-1]=val
        if self.tail==self.size:
            self.tail=0
    def dequeue(self):
        value=self.buffer[self.head]
        self.head+=1
        if self.head>=self.size:
            self.head=0
        self.length-=1
        return value
