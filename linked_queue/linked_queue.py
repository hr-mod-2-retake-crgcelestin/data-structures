# Write your code here to make the tests pass.
#
# Change your working directory to this directory,
# linked_queue.
#
# # Start by running python -m pytest tests/test_01.py and
# making the test pass.
#
# Then, run python -m pytest tests/test_02.py to make the
# next test pass. Keep going to tests/test_15.py.

class LinkedQueueNode():
    def __init__(self, value, link=None):
        self.value=value
        self.link=link
class LinkedQueue():
    def __init__(self, head=None, tail=None, length=0):
        self.head=head
        self.tail=tail
        self.length=length
    def dequeue(self):
        if self.length==0:
            raise(Exception)
        prevHead=self.head
        nextHead=prevHead.link
        self.head=nextHead
        if nextHead==None:
            self.tail=None
        self.length-=1
        return prevHead.value

    def enqueue(self,value):
        newNode=LinkedQueueNode(value)
        if self.length==0:
            self.head=newNode
            self.tail=newNode
        else:
            self.tail.link=newNode
            self.tail=newNode
        self.length+=1

    def traverse(self):
        current=self.head
        i=0
        while i<self.length and current:
            print(current.value)
            current=current.link
            i+=1
        return

# Queue=LinkedQueue()
# Queue.enqueue(1)
# Queue.enqueue(2)
# Queue.enqueue(3)
# # print(Queue.tail.value)
# Queue.traverse()
