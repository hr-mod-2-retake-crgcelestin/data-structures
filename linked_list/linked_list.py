# Write your code here to make the tests pass.
#
# Change your working directory to this directory,
# linked_list.
#
# Start by running python -m pytest tests/test_01.py and
# making the test pass.
#
# Then, run python -m pytest tests/test_02.py to make the
# next test pass. Keep going to tests/test_19.py.

import math
class LinkedListNode():
    def __init__(self, value, link=None):
        self.value=value
        self.link=link

class LinkedList():
    def __init__(self, length=0):
        self.head=None
        self.tail=None
        self.length=length
        @property
        def length(self):
            return self.length

    def insert(self, value, position=None):
        new_node=LinkedListNode(value)
        #if node is empty, ie length of LL is 0
        if self.head is None:
            self.head=new_node
            self.tail=self.head
        #if there are nodes existing already
        #if the index is greater/less than 0 and its less than the LL length
        elif position is not None and position<self.length:
            #unshift
            if position==0:
                new_node.link=self.head
                self.head=new_node
            else:
            #insertion at non-zero position with exisitng nodes
                node=self.traverse(position-1)
                new_node.link=node.link
                node.link=new_node
        else:
        #insert at end
            self.tail.link=new_node
            self.tail=new_node
        self.length+=1

    def get(self,position):
        if position<0:
            position=self.length+position
        node=self.traverse(position)
        return node.value

    def traverse(self, position):
        if position>=self.length:
            raise IndexError('position out of range of list')
        if position<0:
            position=self.length+position
        current_node=self.head
        count=0
        while(count<position):
            current_node=current_node.link
            count+=1
        return current_node

    def shift(self):
        if not self.length:
            return
        prevHead=self.head
        self.head=prevHead.link
        if self.length==0:
            self.tail=None
        self.length-=1
        return prevHead.value

    def remove(self, position):
        if position==0:
            return self.shift()
        if position<0:
            position+=self.length
        current=self.traverse(position-1)
        RemoveNode=current.link
        nextNode=RemoveNode.link
        value=RemoveNode.value
        current.link=nextNode
        if RemoveNode.link==None:
            self.tail=current
        self.length-=1
        return value


    def print_traverse(self):
        current=self.head
        track=0
        if self.length==0:
            return
        if self.length !=0:
            while track<self.length:
                print(current.value)
                current=current.link
                track+=1
        return


# linkedlist=LinkedList()
# linkedlist.insert(100)
# linkedlist.insert(99)
# linkedlist.insert(98)
# print(linkedlist.tail.value)
# linkedlist.remove(3)
# print(linkedlist.tail)
